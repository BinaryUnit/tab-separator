<div align="center">
    <h1 align="center">Tab separator</h1>
    <p>Online tool to make tab separators by changing the document title<p>
    <img src="./tabs.png" alt="Example of browser tabs with the document title change" align="center" />
    <hr>
</div>

Simple online tool to separate a row of browser tabs by changing the document title.
You can try it [here](https://labs.binaryunit.com/tab-separator/) 

## Options
- random (default): picks a random title from a predefined separators
- custom: set a custom tab title

### Using custom parameter

Passing the parameter `?custom=Test` will override the above options so it can be used with a bookmarklet likse so:

```
javascript: (() => {
  window.open("https://labs.binaryunit.com/tab-separator/?custom=Test")
})();
```

